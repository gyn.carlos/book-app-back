<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Exception;


class AuthController extends Controller
{
    /**
     * Display sanctum
     */

    public function login()
    {
        return response()->json([
            'success' => false, 
            'message' => 'Usuário não autenticado',
        ]);
    }

    public function authenticate(Request $request)
    {
        try {

            // Validate fields
            $rules = ['email'=>'required|email', 'password'=>'required'];

            $credentials = $this->validate($request,$rules);

            // Validate credentiais
            if (!auth()->validate($credentials)) {
                $message = [
                    'success' => false, 
                    'message' => 'Credenciais inválidas'
                ];
                return response()->json($message, 401);
            }

            // Check user in database
            $user = User::where('email', $request['email'])->firstOrFail();

            // Create token
            $tokenBearer = [
                'success' => true,
                'access_token' => $user->createToken('auth_token')->plainTextToken,
                'token_type' => 'Bearer',
            ];

            return response()->json($tokenBearer);

        } catch(Exception $e) {
            return response()->json([
                'success' => false, 
                'message' => $e->getMessage()
            ], 403);
        }
    }

    public function revoke(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'success' => true,
            'message' => 'Token revoked',
        ]);        
    }
}
