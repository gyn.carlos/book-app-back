<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClimateController extends Controller
{
    public function search (Request $request)
    {
        $apiKey = env('API_CLIMATE_KEY');
        $url    = env('API_CLIMATE_ENDPOINT');

        list($lat, $lon) = array_values($request->only('lat', 'lon'));

        if (empty($apiKey) || empty($url) || (is_numeric($lat) || is_numeric($lon)) === false) {
            return response()->json([
                'success' => false,
                'message' => 'Credenciais ou EndPoint s�o inv�lido',
            ]);
        }        

        $cmd = $this->createCurl($url, $apiKey, $lat, $lon);
        $res = $this->execCurl($cmd);

        return $res;
    }

    private function createCurl($url, $apiKey, $lat, $lon)
    {
        $cmd = '%s?key=%s&lat=%s&lon=%s';

        return sprintf($cmd, $url, $apiKey, $lat, $lon);
    }

    private function execCurl($cmd)
    {
        return file_get_contents($cmd);
    }
}
