<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Book;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function index(Request $request, $bookId = '') 
    {
        // Busca pelo codigo do livro
        if (is_numeric($bookId)) {
            $book  = Book::find($bookId);
            return response()->json($book);
        }

        $input = $request->only('fullts');

        // Retorna duas paginas apenas
        if (empty($input)) {
            $books  = Book::limit(10)->simplePaginate(5);
            return response()->json($books);
        }

        // Quando o filtro for preeenchido
        list($fulltext) = array_values($request->only('fullts'));

        if (empty(trim($fulltext))) {
            return response()->json([
                'success' => true,
                'message' => 'Nenhum livro encontrado.',
                'data' => []
            ]);
        }

        $sql    = "MATCH(title, description, author) AGAINST (:fulltext)";
        $params = ['fulltext' => $fulltext];
        $books  = Book::whereRaw($sql,$params)->orderBy('id', 'desc')->simplePaginate(10);

        if (empty($books)) {
            return response()->json([
                'success' => true,
                'message' => 'Nenhum livro encontrado.',
                'data' => []
            ]);
        }

        return response()->json($books);
    }

    public function create(Request $request) 
    {
        try {

            // Validate fields
            $rules  = [
                'title'             => 'required|max:255',
                'description'       => 'required|max:500',
                'author'            => 'required|max:255',
                'pages'             => 'required|numeric',
                'registration_at'   => 'required|date',
            ];
    
            $fields  = $this->validate($request, $rules);

            $created = Book::create($fields);
    
            if (!$created) {
                return response()->json([
                    'success' => false,
                    'message' => 'Falha no cadastrado do livro'
                ]);
            }
    
            return response()->json([
                'success' => true,
                'message' => 'Livro cadastrado com sucesso'
            ]);

        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function delete(Int $bookId) 
    {
        try {

            // Validate params
            $fields = ['id'=> $bookId];
            $rules  = ['id'=> 'required|numeric'];

            $validator = Validator::make($fields, $rules);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->errors(),
                ]);
            }

            $deleted = Book::where(['id' => $bookId])->delete();

            if (!$deleted) {
                return response()->json([
                    'success' => false,
                    'message' => 'Falha ao apagar do livro'
                ]);
            }

            return response()->json([
                'success' => true,
                'message' => 'Livro apagado com sucesso'
            ]);

        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $bookId) 
    {
        try {

            // Validate params
            $fields = ['id'=> $bookId];
            $rules  = ['id'=> 'required|numeric'];
    
            $validator = Validator::make($fields, $rules);
    
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->errors(),
                ]);
            }

            // Validate fields
            $rules  = [
                'title'             => 'required|max:255',
                'description'       => 'required|max:500',
                'author'            => 'required|max:255',
                'pages'             => 'required|numeric',
                'registration_at'   => 'required|date',
            ];
    
            $fields = $this->validate($request, $rules);
    
            if (!Book::find($bookId)) {
                return response()->json([
                    'success' => false,
                    'errors' => 'O livro informado não existe',
                ]);
            }
    
            $updated = Book::where(['id' => $bookId])->update($fields);
    
            if (!$updated) {
                return response()->json([
                    'success' => false,
                    'message' => 'Falha ao alterar do livro'
                ]);
            }
    
            return response()->json([
                'success' => true,
                'message' => 'Livro alterado com sucesso'
            ]);

        } catch(ValidationException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }        
    }

}
