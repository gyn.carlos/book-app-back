## About

A API foi construida para controlar livros e foi utilizado um ambiente pr�prio compilado em Ubuntu com docker nativo, apache mod event e PHP 7.4 sem o opcache ativado por padr�o no ambiente de desenvolvimento. Para usar a API � necess�rio que usu�rio seja autorizado.

### Requisitos de instala��o

- Docker e docker-compose
- git
- composer

### Configurando

- Ap�s o fim da instala��o dos requisitos clonar o projeto
  ```shell
    git clone https://gitlab.com/gyn.carlos/book-app-back.git
  ```

- Ao t�rmino da clonagem mudar de diret�rio
  ```shell
    cd book-app-back
  ```

- Criando o dotenv
  ```shell
    cp .env.example .env
  ```

- Ap�s clonar o projeto iniciar os containers
  ```shell    
    docker-compose up -d
    # aguardar o fim do processo
  ```

- Com os containers rodando execute.

> **Aten��o:** Aguardar uns segundos para o MySQL subir para ent�o rodar o migrate/populate, caso contr�rio os migrates podem dar erro.

![](./error_migrate.jpeg)

  ```shell
    composer install
    composer migrate
    composer populate
    composer test
  ```

- Atribuindo permiss�o nos diret�rios de logs
  ```shell
    sudo chown www-data -R storage/framework/
    sudo chown www-data -R storage/logs/
  ```

### Requisitos funcionais da API
- O usu�rio PODE criar um livro. POST /books;
- O usu�rio PODE excluir um livro. DELETE /books;
- O usu�rio PODE atualizar as informa��es de um livro. PUT /books;
- O usu�rio PODE buscar por um livro por seu nome, descri��o, autor ou c�digo. GET /books;
- O usu�rio DEVE se autenticar na plataforma. /auth;
  ```
    email: demo@book.com
    password: demo
  ```
- O usu�rio PODE se deslogar da plafaforma. /logout;
- O usu�rio PODE acessar as informa��o de clima tempo de uma regi�o. /climate;

### Documenta��o/Test

- Postman
- Para maiores informa��es por favor baixe a documenta��o [Book API](./Book_API.postman_collection.json) e importe no Postman
