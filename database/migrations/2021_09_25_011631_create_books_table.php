<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title', 225)->comment('T�tulo');
            $table->string('description', 500)->comment('Descri��o');
            $table->string('author', 255)->comment('Autor');
            $table->integer('pages')->comment('N�mero de P�ginas');
            $table->date('registration_at')->comment('Data de Cadastro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
