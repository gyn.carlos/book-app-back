<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        (new User)->create([
            'name' => 'Usuário de demostração',
            'email' => 'demo@book.com',
            'password' => bcrypt('demo'),
        ]);
    }
}
