<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

use Tests\TestCase;

class BookTest extends TestCase
{
    # Clean table
    protected function setUp() : void
    {
        parent::setUp();

        (new Book)->delete();
    }

    /**
     * Auth User
     */
    protected function autenticate()
    {
        // attempt login
        $response = $this->json('POST','/api/auth', [
            'email' => 'demo@book.com',
            'password' => 'demo'
        ]);

        return $response->json();
    }  

    /**
     * @test
     *
     * Test Create Book
     */
    public function testCreateBook()
    {
        // Get token
        $token = $this->autenticate();

        $this->assertArrayHasKey('access_token',$token);

        // Header
        $headers  = ['Authorization' => 'Bearer '. $token['access_token']];

        // Query
        $postData = [
            'title' => 'Programação com café',
            'description' => 'Programação com TDD',
            'author' => 'Carlos Augusto',
            'pages' => '10',
            "registration_at" => "2021-01-01 16:00:00"
        ];
        $response = $this->withHeaders($headers)->json('POST','/api/books', $postData);

        // Assert token
        $response->assertStatus(200);
    }
    
    /**
     * @test
     *
     * Test Select Full Text Book
     */
    public function testSelectBook()
    {
        // Get token
        $token = $this->autenticate();

        $this->assertArrayHasKey('access_token',$token);

        // Header
        $headers  = ['Authorization' => 'Bearer '. $token['access_token']];

        // Query
        $query    = "Programação";
        $response = $this->withHeaders($headers)->json('GET',"/api/books?fullts=$query", []);

        // Assert token
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * Test Update Book
     */
    public function testUpdateBook()
    {
        // Get token
        $token = $this->autenticate();

        $this->assertArrayHasKey('access_token',$token);

        // Header
        $headers  = ['Authorization' => 'Bearer '. $token['access_token']];

        // Query
        $putData = [            
            'title' => 'Programação com café com a presença de leite',
            'description' => 'Programação com TDD',
            'author' => 'Carlos Augusto',
            'pages' => '10',
            "registration_at" => "2021-01-01 16:00:00"
        ];

        $response = $this->withHeaders($headers)->json('PUT',"/api/books/1", $putData);

        // Assert token
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * Test Update Book
     */
    public function testDeleteBook()
    {
        // Get token
        $token = $this->autenticate();

        $this->assertArrayHasKey('access_token',$token);

        // Header
        $headers  = ['Authorization' => 'Bearer '. $token['access_token']];

        $response = $this->withHeaders($headers)->json('DELETE',"/api/books/1", []);

        // Assert token
        $response->assertStatus(200);
    }    
}
