<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * @test
     *
     * Test Login User
     */
    public function testAuthorizateUser()
    {
        // attempt login
        $response = $this->json('POST','/api/auth', [
            'email' => 'demo@book.com',
            'password' => 'demo'
        ]);

        // Assert token
        $response->assertStatus(200);
        $this->assertArrayHasKey('access_token',$response->json());
    }
}
