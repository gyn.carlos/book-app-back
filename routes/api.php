<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\ClimateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// login e revoke
Route::post('/auth', [AuthController::class, 'authenticate']);
Route::post('/logout', [AuthController::class, 'revoke'])->middleware('auth:sanctum');

// climate
Route::get('/climate', [ClimateController::class,'search']);

// redirecting sanctum
Route::get('/login',  [AuthController::class, 'login'])->name('login');

// Book
Route::middleware('auth:sanctum')->group(function() {
    // books
    Route::get('/books',         [BookController::class, 'index']);
    Route::get('/books/{id}',    [BookController::class, 'index']);
    Route::post('/books',        [BookController::class, 'create']);
    Route::put('/books/{id}',    [BookController::class, 'update']);
    Route::delete('/books/{id}', [BookController::class, 'delete']);
});